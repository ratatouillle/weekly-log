<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_
</div>


<br> <br> <div align = "center">
<img src = "Photos/logo_upm.png"> <br> <br>

## 
### UNIVERSITI PUTRA MALAYSIA <br> <br> AEROSPACE DESIGN PROJECT (EAS4947) <br>
## 
<br>

# :rocket: WEEKLY LOG :rocket: </div>
<br>

## Table of Content

| Week | Hyperlink |
| :------: | :------: |
| 01 | [Link](#log-week-01) |
| 02 | [Link](#log-week-02) |
| 03 | [Link](#log-week-03) |
| 04 | [Link](#log-week-04) |
| 05 | [Link](#log-week-05) |
| 06 | [Link](#log-week-06) |
| 07 | [Link](#log-week-07) |
| 08 | [Link](#log-week-08) |
| 09 | [Link](#log-week-09) |
| 10 | [Link](#log-week-10) |
| 11 | [Link](#log-week-11) |
| 12 | [Link](#log-week-12) |
| 13 | [Link](#log-week-13) |
| 14 | [Link](#log-week-14) |

# 




<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br>22 OCTOBER 2021 :cry:
</div>

## Log Week 01

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> The agenda in Week 1 is about the introduction of the Aerospace Design Project and the group assignment. After being assigned to subsystem group, we have been briefly introduced about the task for each subsystem. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> Null. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> Null. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> Null. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> Null. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We are waiting for next instruction from Dr. Salah and briefing from Mr. Fikri to clear up our mind about the task of each subsystem. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br>29 OCTOBER 2021 :confused:
</div>

## Log Week 02

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> The agenda for Week 2 is solving the assignment 1 and assignment 2 given by Mr. Azizi. We also need to planned our task for each subsystem and came out with the Gantt Chart. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> We decide to list out the progress of each subsystem in the same Google Spreadsheet to ease all of us reach each subsystem progress. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> We decide to make one Google Spreadsheet after discussing in the voice channel during class hour. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> We choose to us Google Spreadsheet of the Gantt Chart to ease every students to look out every subsystem progress. Besides, it also ease updating works by each subsystem so that it does not overlapped with each other. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> Our Airship Progress look very nice and tidy. All students can just reach out the same link to see other subsystem Gantt Chart. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We are planning to visit laboratory H2.2 to observe the thrust test setup and equipment. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br>5 NOVEMBER 2021 :imp:
</div>

## Log Week 03

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> Week 3 agenda is to disassemble the motor and esc from the old thruster arms so that we can continue with thrust test. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> We decide to tested one motor by using two size propeller, 16 inch and 24 inch. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> We planned to test the motor and ESC by using thrust test. The specification of the motor already on the websites but we need to know the current condition of the motor. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> We decided to undergoes the thrust test after being adviced by Mr. Fikri. to access the condition of the motor. We also need to familiarize with the thrust test so that we can undergoes thrust test for the new motor later on. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> We got to learn how to conduct thrust test and it is beneficial for us to test out the condition of the motor. We can compare the result from the thrust test and the specification provided by the manufacturer. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We have been assigned by Mr. Fikri to find out the maximum thrust for the airship. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br>12 NOVEMBER 2021 :face_with_thermometer:
</div>

## Log Week 04

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> We need to find out the maximum thrust for the airship when it is in the air. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> We are planning to undergoes trolley test. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> We will use a trolley then we put the airship filled with normal air on that trolley. Then, we will pulled the trolley in fixed distance with a fixed amount of time. The spring constant is attached to the rope so that we will get the force exerted on the trolley. We also record the time from the starting point until the ending point. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> After the trolley test, we decided not to used the data because there are lot of uncertainties in this experiment such as the amount of forces used, the direction and magnitude of force, friction forces of the trolley's roller, volume of air inside the airship etc. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> We are trying to figure out other ways to calculate the maximum thrust for the airship. After consulting with Dr. Ezanee, he suggest us to calculate the thrust using thrust formula and the Cl &  Cd are provided by Simulation Team Subsystem. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We need to revised the theorectical for the thrust calculation needed for the maximum thrust. We also need to test all of the previous airship motor and ESC. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br> :smiling_imp:
</div>

## Log Week 05

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> We need to undergoes the thrust test for all four motor from the previous airship. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> We are planning to come to the lab H2.2 on Wednesday to complete the thrust test. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> We are planning to come to the lab on the WhatsApp group and all of us agreed to come on Wednesday. We will undergoes the thrust test under supervision Dr. Ezanee. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> We need to complete the thrust test to figure out the thrust produce by the old motor. We also need to know whether the motor can be used or not. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> Team members are familiar with the setup and operate the thrust test. Thus, we can reduced the amount of errors when testing the new motor. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We are planning to analyse the data from the thrust test and come up with a graph. Dr Salah also suggest us to test the best motor using battery because the power supply's voltage is not similar to the battery's voltage. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br> :mask:
</div>

## Log Week 06

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> The agenda for Week 6 is to test out the Motor No. 2 with the ESC No. 4 with the battery. The thrust value for the thrust test will be recorded and the data will be plotted. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> We decided to go to the laboratory on Wednesday. During the thrust test, we encounter with a problems with the thrust test. The recorded data is not stable and have high noise. After trying to change the motor, esc and propeller direction, we planned to take a break since all of us seems quite tired. After 30 minutes break, we trying to figure out the problems and fortunately we found the source of the problems. The problems come from one of the ESC connection to the thrust test's board which are not properly connected. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> We observed the pattern of the data come out from the thrust test and we also notice that the motor is rotate until certain speed then it stuck. The vibration also seems very high than normal. Thus, we tried to overlook again so that our test will get the result that we want. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> We decided to check the connection of the wire because we already check the motor, ESC, power supply and the propeller but these equipments are in the best state. Thus, we assume that the problem come from the connection of the wire. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> When we figure out the connection is the cause of the problems, we can continue the thrust test and get the result that we wanted. The motor worked correctly and can go up to 100 percent throttle with lower vibration. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We planned to plot the graph from the data that we have already obtained. We also planned to calculate the maximum thrust that the motor can reach by using Cl and Cd from the Simulation Team Subsystem. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br> :upside_down:
</div>

## Log Week 07

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> The agenda for Week 7 is to find out the vibration noise from the Week 6 data. We also want to discuss certain things regarding the propulsion of the airship with Dr. Ezanee. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> We decided to schedule an online meeting with Dr. Ezanee since not all of us can reach lab on the same time. We planned to ask and discuss with Dr. Ezanee in online discussion. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> We make those decision after discussing in the WhatsApp group. At first, we planned to go to the lab on Wednesday but not all of us can reach the lab on the same time, thus we came out with another plan to schedule an online meeting so that most of us can joined the discussion. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> Our team make that choice to ensure that all of us can get a chances to actively participate in the discussion with Dr. Ezanee. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> Everyone in the propulsion team will get a chances to participate in the online discussion and directly asked to Dr. Ezanee what is on their mind. It will increase the effectiveness of propulsion group's work and all students will be on the same page. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We are planning to wait for the new motor, esc, and battery to arrive so that we can assemble and test it out. In the mean time, we trying to figure out how to land the airship. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br> :sleeping:
</div>

## Log Week 08

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> Week 8 goal is to assemble all the component that have been arrived recently. The propulsion team is keep in touch with other subsytems incase other subsystems need any help. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> There were no problems need to be solve because propulsion team act as a backup team to others. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> Propulsion team always keep update with others so that we get notified faster incase others need any help. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> In this wasy, other group will get help faster if anything happens. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> The works in this week is going smoothly without any interuption because there is always a backup team to help. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> Next week, Dr. Salah allows us to run a test on the new motor to get the thrust and rpm for 30 percent throttle. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br> :zipper_mouth_face:
</div>

## Log Week 09

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> Week 9 agenda is to test the new motor that arrived along with the ESC and the battery. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> We were planned to undergo the test on Monday since the Flight System Integration subsystem will assemble the component on that day. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> The method is thrust test need to be undergoes to test the 30 percent of the throttle for the motor. The data can be analysed from RCbenchmark software. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> We were successfully conducting the thrust test with the help from Dr. Ezanee to get the 30 percent from the full throttle for the motor. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> We already get the result from the thrust test which is around 1kg of thrust when the motor reach 30 percent of throttle. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> The motor, ESC and the battery will be assembled to the main board and the result will be shared to the whole class. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br> :rage:
</div>

## Log Week 10

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> Week 10 goal is to working on the Propulsion and Power Report and resume since there is no class for Week 10 regarding the flood issues. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> We have discussed in the WhatsApp group to divide the work. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> Our subsystem has devided the report into a few section to make sure everyone is contributing to the report. We have made a working list in the WhatsApp group and everyone need to fill in their name to ensure there is no overlapping work with each other. We also created an online docs which everyone can update everything on the same docs. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> We want to avoid any overlapping works and using the same online docs will always keep the latest update for everyone. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> There is no overlap work between each members of Propulsion and Power subsystem which wasting the time. The report also become more organized. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We will be a backup team for other subsystem incase they need help for there task especially Flight System and Integration team. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br> :expressionless:
</div>

## Log Week 11

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> The agenda for Week 11 is to finished the assembly of the electrical circuit for the airship. The works is completed by Flight System and Integration subsystem but Propulsion and Power subsystem will act as backup incase they short-handed of workforce. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> We have decided to help Flight System and Integration team in completing their task on the assembly of the electrical components. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> We help Flight System and Integaration team to stick the patch on the airship body, procurement of 16AWG connecting wire, cutting the connecting wire, etc. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> We help Flight System and Integration team because few of their team members are unable to work on the task since they need to quarantine and some are not free. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> We managed to keep the progress on track and avoid any delay on the project. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We still backup the Flight System and Integration team until the airship is able to fly. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br> :face_with_rolling_eyes:
</div>

## Log Week 12

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> Week 12 goal is preparing a short report and help other subsystem to finish their task so that the flight test can be done. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> Some of us are going to tie the tent so that the stand is strong enough to hold still to the Earth. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> We choose to tie the tent's stand to the stone brick to give some weight. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> The mass from the brick will add some weight to the tent's stand which can hold it stronger during thunderstorm and high velocity wind. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> After few days, the tent was still stand strong with the added weight from the bricks. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We are waiting for the final assembly and uploading the firmware into the control board. Then, we can undergoes the flight test after everything is finish. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br> :triumph:
</div>

## Log Week 13

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> We targeting to assemble all the propulsion and power equipment such as the motor, esc, battery and the propeller. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> We decided to go to lab to assemble the components. We need to work with other subsystems to assemble the components because it related to other subsystem such as control subsystem and flight system integration subsystem. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> We gathered at the lab with at least one representative from each subsystem to assemble the components to make sure all of us is updated with the current progress from each subsystems timeline. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> With one each representative from each subsystem, we can discuss on the spot to solve any problem that arised and make the progress of each subsystem more efficient.</div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> The works literally organised without overlap with each other. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> We are targetting to fly the airship on Friday of the Week 14. </div> |

[Return to Table of Content](#table-of-content)

# 
<div align = "right">

###### _AEROSPACE DESIGN PROJECT, EAS4947 <br>SEMESTER 1, SESSION 2021/2022_ <br> :unamused:
</div>

## Log Week 14

| No | Content | Description |
| :------: | :------: | :------: |
| 1 | <div align = "left"> Agenda/Goals </div> | <div align = "justify"> We just have few more days before fly the airship, thus we need to make sure the progress of each subsystems are following the timeline. We need to cover other subsystem such as flight system and integration as well as control subsystem to avoid any delay and minor problems that arised during the progress. </div> |
| 2 | <div align = "left"> Decision to solve problems </div> | <div align = "justify"> From previous thrust test, we noticed that the shaft of the new motor is rotating and it will causing spark because it will touch the thruster arm. We need to avoid the shaft from touching the thruster arm while it rotating. </div> |
| 3 | <div align = "left"> Method to solve problems </div> | <div align = "justify"> We added four washers at every screw in between the motor and the thruster arm to leave some space. </div> |
| 4 | <div align = "left"> Justification of those decision </div> | <div align = "justify"> By adding the washer, the rotating washer will not touching the thruster arm which will not causing any spark. </div> |
| 5 | <div align = "left"> Impact from those decision </div> | <div align = "justify"> We avoiding the rotating shaft behind the motor from touching the thruster arms. We also notified other subsystem so that they were aware of the rotating shaft. </div> |
| 6 | <div align = "left"> Next step </div> | <div align = "justify"> After working on the airship for 14 weeks, we are ready to fly the airship for the first time. We are just hoping that the airship can fly and move forward as well as backward without any problems. We did not expecting much since all of us are inexperienced but with the guidance from Dr. Salah, Mr. Azizi, Dr. Ezanee, and Mr. Fiqri, we are able to give our best. </div> |

[Return to Table of Content](#table-of-content)
